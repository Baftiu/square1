@extends('layouts.app')
@section('content')   
        <div class="content">
        <div class="row justify-content-center">
            <div class="col-md-6 mt-3">
                <div class="card">
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Title</th>
                            <th scope="col">Description</th>
                            <th scope="col">Publication date</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post['title']}}</td>
                                <td>{{$post['description']}}</td>
                                <td>{{$post['publication_date']}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </div>
@endsection