@extends('layouts.app')

@section('content')
<div class="row">
    <div class="offset-md-2 col-md-8">
        <div class="card border-primary mb-3">
            <h5 class="card-header">Create Post</h5>
            <div class="card-body">
                <div class="col-md-8 offset-md-2">
                    <form action="{{ action('PostController@store')}}" method="post" 
                        class="needs-validation" novalidate>
                        @csrf
                        <div class="form-group">
                            <div class="row">
                                    <label for="title" 
                                        class="col-form-label text-left mt-1"> Title</label>
                                        <input id="title" type="title"
                                        class="form-control"
                                        name="title"
                                        required
                                        autocomplete="title">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                    <label for="description"
                                        class=" col-form-label text-left mt-1">Description: </label>
                                    <input id="description" type="description"
                                        class="form-control"
                                        name="description"
                                        required
                                        autocomplete="description">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="publication_date">Publication date</label>

                            <input type="date" name="publication_date" id="datepicker" class="form-control mb-5 ml-1">
                            <button type="submit" class="btn btn-primary">Add post</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection