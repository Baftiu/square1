@extends('layouts.app')

@section('content')
<div class="container">
        <h2 class="page-title text-white">Posts</h2>
        <form method="GET" action="{{ action('PostController@index') }}" style="display: flex;">
            <input type="text" value="{{ request('search') }}" name="search" class="form-control ml-2" id="search">
            <button type="submit" class="btn btn-info btn-sm ml-2 "><i class="material-icons"></i> Search</button>
        </form>
        @auth()
            <a class="btn btn-secondary pull-left" href="{{action('PostController@create')}}">New Post</a>
        @endauth
    <div class="row justify-content-center">
        <div class="col-md-12 mt-3">
            <div class="card">
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col-md-3">Title</th>
                        <th scope="col-md-4">Description</th>
                        <th scope="col-md-5">Publication date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post['title']}}</td>
                                <td>{{$post['description']}}</td>
                                <td>{{$post['publication_date']}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
