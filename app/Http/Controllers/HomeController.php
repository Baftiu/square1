<?php

namespace App\Http\Controllers;

use App\Http\Resources\Home;
use App\Post;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $apiPosts['data'] = [];
        $posts = Post::orderBy('publication_date')->get();

        if(! auth()->check()){
            $apiPosts = collect(Http::get('https://sq1-api-test.herokuapp.com/posts')->json());
        }
        return view('welcome', [
            'posts' => array_merge($posts->toArray(),$apiPosts['data']),
        ]);
    }
    
    public function search()
    {
        $posts = Post::latest();        

        if(request()->filled('search')){
            $posts->where('title','like','%'.request('search').'%')->get();
        }
        return view('home');
    }
}
